﻿using Renci.SshNet;
using Renci.SshNet.Common;
using Renci.SshNet.Sftp;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SFTPer
{
    public enum SFTPAuthenticationMethod
    {
        KeyboardInteractive,
        Password
    }

    public class SFTPer
    {
        #region Properties

        public string hostName { get; set; }
        public string userName { get; set; }
        public string passWord { get; set; }
        public int portNumber { get; set; }
        //private int reTries { get; set; }
        private bool stopOnFailure { get; set; }
        private bool overwriteDestination { get; set; }

        SFTPAuthenticationMethod AuthMethod { get; set; }
        public SftpClient sftpClient;

        #endregion


        public SFTPer(string hostName, string userName, string passWord, int portNumber, bool stopOnFailure, SFTPAuthenticationMethod sftpAuthenticationMethod)
        {
            this.hostName = hostName;
            this.userName = userName;
            this.passWord = passWord;
            this.portNumber = portNumber;
            this.stopOnFailure = stopOnFailure;
            this.AuthMethod = sftpAuthenticationMethod;
        }

        private void HandleKeyEvent(object sender, AuthenticationPromptEventArgs e)
        {
            foreach (AuthenticationPrompt prompt in e.Prompts)
            {
                if (prompt.Request.IndexOf("Password:", StringComparison.InvariantCultureIgnoreCase) != -1)
                {
                    prompt.Response = this.passWord;
                }
            }
        }

        private void Connection_Setup()
        {
            PasswordAuthenticationMethod PWAuthMeth = new PasswordAuthenticationMethod(this.userName, this.passWord);
            Renci.SshNet.ConnectionInfo connInfo = new Renci.SshNet.ConnectionInfo(this.hostName, this.portNumber, this.userName, PWAuthMeth);

            if (this.sftpClient == null)
                sftpClient = new SftpClient(connInfo);

            if (this.AuthMethod == SFTPAuthenticationMethod.KeyboardInteractive)
            {
                KeyboardInteractiveAuthenticationMethod keybAuth = new KeyboardInteractiveAuthenticationMethod(this.userName);
                keybAuth.AuthenticationPrompt += new EventHandler<AuthenticationPromptEventArgs>(HandleKeyEvent);

            }
        }

        public void Connect()
        {
            this.Connection_Setup();
            this.sftpClient.Connect();
        }

        /// <summary>
        /// Uploads the files.
        /// </summary>
        /// <param name="destinationFilePath">Full source path</param>
        /// <param name="sourceFilePath">Full destination path</param>
        /// <exception cref="System.Exception">Remote File Already Exists.</exception>
        public void UploadFiles(string sourceFilePath, string destinationFilePath)
        {
            try
            {
                FileInfo fileInfo = new FileInfo(sourceFilePath);
                if (destinationFilePath.EndsWith("/"))
                    destinationFilePath = Path.Combine(destinationFilePath, fileInfo.Name).Replace(@"\", "/");

                if (this.sftpClient.Exists(destinationFilePath))
                {
                    if (overwriteDestination)
                        this.sftpClient.Delete(destinationFilePath);
                    else
                    {
                        if (this.stopOnFailure)
                            throw new Exception("Remote File Already Exists.");
                    }
                }

                using (FileStream file = File.OpenRead(sourceFilePath))
                    this.sftpClient.UploadFile(file, destinationFilePath);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Upload: ", ex);
            }
        }

        /// <summary>
        /// Downloads the files.
        /// </summary>
        /// <param name="remoteFullFilePath">The full path to the remote file to download.</param>
        /// <param name="localDestinationPath">Local destination path -- either file or folder</param>
        /// <exception cref="System.Exception">
        /// Local File Already Exists.
        /// </exception>
        public void DownloadFiles(string remoteFullFilePath, string localDestinationPath, bool deleteRemote=false)
        {
            try
            {
                if (!this.sftpClient.Exists(remoteFullFilePath))
                {
                    if (this.stopOnFailure)
                        throw new Exception(String.Format("Remote Path Does Not Exist: [{0}]", remoteFullFilePath));
                }

                if (Path.GetDirectoryName(localDestinationPath) == localDestinationPath.TrimEnd('\\'))
                    localDestinationPath = Path.Combine(localDestinationPath, remoteFullFilePath.Substring(remoteFullFilePath.LastIndexOf("/") + 1));
                
                // Can we overwrite the local file
                if (!this.overwriteDestination && File.Exists(localDestinationPath))
                    throw new Exception("Local File Already Exists.");

                using (FileStream fileStream = File.OpenWrite(localDestinationPath))
                    this.sftpClient.DownloadFile(remoteFullFilePath, fileStream);

                if (deleteRemote)
                    this.DeleteFile(remoteFullFilePath);
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to Download: ", ex);
            }
        }

        /// <summary>
        /// Lists the files.
        /// </summary>
        /// <param name="remotePath">The remote path.</param>
        /// <returns></returns>
        /// <exception cref="System.Exception"></exception>
        public List<SftpFile> ListFiles(string remotePath = "/")
        {
            if (remotePath == "") remotePath = "/";

            List<SftpFile> fileList = new List<SftpFile>();
            try
            {
                if (!this.sftpClient.Exists(remotePath))
                {
                    if (this.stopOnFailure)
                        throw new Exception(String.Format("Invalid Path: [{0}]", remotePath));
                }
                else
                {
                    SftpFile sftpFileInfo = this.sftpClient.Get(remotePath);
                    if (sftpFileInfo.IsDirectory)
                    {
                        IEnumerable<SftpFile> dirList = this.sftpClient.ListDirectory(remotePath);
                        foreach (SftpFile sftpFile in dirList)
                            fileList.Add(sftpFile);
                    }
                    else
                        fileList.Add(sftpFileInfo);
                }
            }
            catch (Exception ex)
            {
                throw new Exception("Unable to List: ", ex);
            }

            return fileList;
        }

        /// <summary>
        /// Deletes a file from the sftp site
        /// </summary>
        /// <param name="destinationFilePath">Full source path</param>
        public void DeleteFile(string remoteFullFilePath)
        {
            this.sftpClient.DeleteFile(remoteFullFilePath);
        }
    }
}
